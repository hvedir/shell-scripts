#!/usr/bin/env php
<?php
$opts = getopt('i:o:',array('input-har:','output-dir:'));

$inputFile = isset($opts['i'])?$opts['i']:(isset($opts['input-har'])?$opts['input-har']:null);
if (!$inputFile) {
    die (PHP_EOL.'Not provided required option [-i|--input-har]'.PHP_EOL);
}

$outputDir = isset($opts['o'])?$opts['o']:(isset($opts['output-dir'])?$opts['output-dir']:preg_replace('/\.[a-z]+$/i', '', $inputFile));

if (file_exists($outputDir) && !is_dir($outputDir)) {
    $outputDirCandidat = $outputDir;
    $i = 0;
    do {
        ++$i;
        $outputDirCandidat = $outputDir + '_' + $i;
    } while (file_exists($outputDirCandidat) && !is_dir($outputDirCandidat));
    $outputDir = $outputDirCandidat;
}
if (!file_exists($outputDir)){
    mkdir($outputDir, 0775, true);
}

echo 'Working dir: '.$outputDir.PHP_EOL.PHP_EOL.'Files created:'.PHP_EOL;

$parsedHar = json_decode(file_get_contents($inputFile), true);


$baseDomain = preg_replace('@^(http(s?)\:/[a-z\.0-9_\-]+(\:\d+)?/)@i', '$1', $parsedHar['log']['pages'][0]['title']);
$parsedHar = $parsedHar['log']['entries'];

foreach ($parsedHar as $entry) {
    $file = str_replace($baseDomain, '', $entry['request']['url']);
    if ($entry['response']['status'] !== 200 || strlen($file) == strlen($entry['request']['url'])) {
        continue;
    }
    $file = $outputDir.'/'. ($file === ''?'index.html':$file);
    $dir  = dirname($file);
    if (!file_exists($dir)) {
        mkdir($dir, 0775, true);
    }
    $content = $entry['response']['content']['text'];
    if (!preg_match('/(text|json|javascript)/i',$entry['response']['content']['mimeType'])) {
        $content = base64_decode($content);
    }
    file_put_contents($file, $content, FILE_TEXT);
    echo '* file "'+$file+'" added'.PHP_EOL;
}
echo PHP_EOL.'Done'.PHP_EOL.PHP_EOL;


